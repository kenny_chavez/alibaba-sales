﻿Imports MySql.Data.MySqlClient

Public Class categorie_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub refreshData()
        MainLayout.Hide()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT * FROM categoria", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub

    Private Sub categorie_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        refreshData()
    End Sub

    Private Sub btnGoCategories_Click(sender As Object, e As EventArgs) Handles btnGoCategories.Click
        Categories.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class