﻿Imports MySql.Data.MySqlClient

Public Class products_layout
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub brands_layout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        LoadBrands()
        LoadCategories()
        refreshData()
    End Sub

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT p.idProducto, p.Producto, p.Precio, p.Stock, p.Descripcion, c.Categoria, m.Marca FROM producto as p
            INNER JOIN categoria c ON p.Categoria_idCategoria = c.idCategoria 
            INNER JOIN marca m ON p.Marca_idMarca = m.idMarca", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub

    Private Sub LoadBrands()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM marca", connection)
            READER = COMMAND.ExecuteReader
            cbBrand.ValueMember = "Key"
            cbBrand.DisplayMember = "Value"

            Dim cbBrandValues As New Dictionary(Of String, String)

            While READER.Read
                Dim brandName = READER.GetString("Marca")
                Dim id = READER.GetString("idMarca")

                cbBrandValues.Add(id, brandName)
            End While

            cbBrand.DataSource = New BindingSource(cbBrandValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub LoadCategories()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM categoria", connection)
            READER = COMMAND.ExecuteReader
            cbCategorie.ValueMember = "Key"
            cbCategorie.DisplayMember = "Value"

            Dim cbCategorieValues As New Dictionary(Of String, String)

            While READER.Read
                Dim categorieName = READER.GetString("Categoria")
                Dim id = READER.GetString("idCategoria")

                cbCategorieValues.Add(id, categorieName)
            End While

            cbCategorie.DataSource = New BindingSource(cbCategorieValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddProduct = "INSERT INTO producto(Producto, Precio, Stock, Descripcion, Categoria_idCategoria, Marca_idMarca) VALUES 
        ('" + txtName.Text + "', " + txtPrice.Text + ", " + txtStock.Text + ", '" + rtxtDescription.Text + "', " + cbCategorie.SelectedValue.ToString + ", " + cbBrand.SelectedValue.ToString + ")"
        MakeQuery(QueryAddProduct)
        refreshData()

        txtName.Text = ""
        txtPrice.Text = ""
        txtStock.Text = ""
        rtxtDescription.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtProducts.SelectedRows.Count > 0 Then
            For i As Integer = dtProducts.SelectedRows.Count - 1 To 0 Step -1

                Dim ProductId As String

                ProductId = dtProducts.Rows(dtProducts.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM producto WHERE idProducto = " + ProductId
                MakeQuery(queryDelete)

                dtProducts.Rows.RemoveAt(dtProducts.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtProducts.ReadOnly = False
    End Sub

    Private Sub dtProducts_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtProducts.CellValueChanged
        Dim id As String = dtProducts.CurrentRow.Cells(0).Value
        Dim value As String = dtProducts.CurrentCell.Value
        Dim x As String = dtProducts.CurrentCellAddress.X
        Dim queryEdit As String = "UPDATE producto SET "

        If x.Equals("1") Then
            queryEdit += " Producto='" + value + "' "
        End If

        If x.Equals("2") Then
            queryEdit += " Precio=" + value + " "
        End If

        If x.Equals("3") Then
            queryEdit += " Stock=" + value + " "
        End If

        If x.Equals("4") Then
            queryEdit += " Descripcion='" + value + "' "
        End If

        If x.Equals("5") Then
            queryEdit += " Categoria_idCategoria=" + value + " "
        End If

        If x.Equals("6") Then
            queryEdit += " Marca_idMarca=" + value + " "
        End If

        queryEdit += " WHERE idProducto = " + id

        If x <> "0" Then
            MakeQuery(queryEdit)
        End If

        refreshData()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class