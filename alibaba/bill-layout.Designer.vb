﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class bill_layout
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.cbProducts = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbBills = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.dtBills = New System.Windows.Forms.DataGridView()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtHeader = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.calendar = New System.Windows.Forms.MonthCalendar()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbClients = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnSaveDetail = New System.Windows.Forms.Button()
        Me.btnEditDetail = New System.Windows.Forms.Button()
        Me.btnDeleteDetail = New System.Windows.Forms.Button()
        Me.dtDetail = New System.Windows.Forms.DataGridView()
        Me.btn5Clients = New System.Windows.Forms.Button()
        Me.btn10Products = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnProducts = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbPlace = New System.Windows.Forms.ComboBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dtBills, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HomeToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(800, 24)
        Me.MenuStrip1.TabIndex = 23
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HomeToolStripMenuItem
        '
        Me.HomeToolStripMenuItem.Name = "HomeToolStripMenuItem"
        Me.HomeToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.HomeToolStripMenuItem.Text = "Home"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(695, 598)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 57
        Me.btnBack.Text = "Atrás"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'cbProducts
        '
        Me.cbProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProducts.FormattingEnabled = True
        Me.cbProducts.Location = New System.Drawing.Point(86, 457)
        Me.cbProducts.Name = "cbProducts"
        Me.cbProducts.Size = New System.Drawing.Size(191, 21)
        Me.cbProducts.TabIndex = 56
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(32, 460)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 55
        Me.Label7.Text = "Producto"
        '
        'cbBills
        '
        Me.cbBills.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbBills.FormattingEnabled = True
        Me.cbBills.Location = New System.Drawing.Point(86, 430)
        Me.cbBills.Name = "cbBills"
        Me.cbBills.Size = New System.Drawing.Size(191, 21)
        Me.cbBills.TabIndex = 54
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(32, 433)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 53
        Me.Label6.Text = "idFactura"
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(635, 241)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(124, 23)
        Me.btnEdit.TabIndex = 46
        Me.btnEdit.Text = "Habilitar Edición"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(554, 241)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(76, 23)
        Me.btnDelete.TabIndex = 45
        Me.btnDelete.Text = "Eliminar"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'dtBills
        '
        Me.dtBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtBills.Location = New System.Drawing.Point(400, 71)
        Me.dtBills.Name = "dtBills"
        Me.dtBills.ReadOnly = True
        Me.dtBills.Size = New System.Drawing.Size(359, 164)
        Me.dtBills.TabIndex = 44
        Me.dtBills.UseWaitCursor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(202, 351)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 43
        Me.btnSave.Text = "Guardar"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtHeader
        '
        Me.txtHeader.Location = New System.Drawing.Point(99, 68)
        Me.txtHeader.Name = "txtHeader"
        Me.txtHeader.Size = New System.Drawing.Size(178, 20)
        Me.txtHeader.TabIndex = 42
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Encabezado"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 24)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Factura"
        '
        'calendar
        '
        Me.calendar.Location = New System.Drawing.Point(29, 177)
        Me.calendar.Name = "calendar"
        Me.calendar.TabIndex = 58
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 158)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Fecha"
        '
        'cbClients
        '
        Me.cbClients.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClients.FormattingEnabled = True
        Me.cbClients.Location = New System.Drawing.Point(99, 94)
        Me.cbClients.Name = "cbClients"
        Me.cbClients.Size = New System.Drawing.Size(178, 21)
        Me.cbClients.TabIndex = 61
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(26, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Cliente"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(25, 393)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(135, 24)
        Me.Label5.TabIndex = 62
        Me.Label5.Text = "Detalle Factura"
        '
        'txtQuantity
        '
        Me.txtQuantity.Location = New System.Drawing.Point(87, 484)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(190, 20)
        Me.txtQuantity.TabIndex = 64
        Me.txtQuantity.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(32, 487)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 63
        Me.Label8.Text = "Cantidad"
        '
        'btnSaveDetail
        '
        Me.btnSaveDetail.Location = New System.Drawing.Point(202, 520)
        Me.btnSaveDetail.Name = "btnSaveDetail"
        Me.btnSaveDetail.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveDetail.TabIndex = 65
        Me.btnSaveDetail.Text = "Guardar"
        Me.btnSaveDetail.UseVisualStyleBackColor = True
        '
        'btnEditDetail
        '
        Me.btnEditDetail.Location = New System.Drawing.Point(635, 534)
        Me.btnEditDetail.Name = "btnEditDetail"
        Me.btnEditDetail.Size = New System.Drawing.Size(124, 23)
        Me.btnEditDetail.TabIndex = 68
        Me.btnEditDetail.Text = "Habilitar Edición"
        Me.btnEditDetail.UseVisualStyleBackColor = True
        '
        'btnDeleteDetail
        '
        Me.btnDeleteDetail.Location = New System.Drawing.Point(554, 534)
        Me.btnDeleteDetail.Name = "btnDeleteDetail"
        Me.btnDeleteDetail.Size = New System.Drawing.Size(76, 23)
        Me.btnDeleteDetail.TabIndex = 67
        Me.btnDeleteDetail.Text = "Eliminar"
        Me.btnDeleteDetail.UseVisualStyleBackColor = True
        '
        'dtDetail
        '
        Me.dtDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtDetail.Location = New System.Drawing.Point(400, 364)
        Me.dtDetail.Name = "dtDetail"
        Me.dtDetail.ReadOnly = True
        Me.dtDetail.Size = New System.Drawing.Size(359, 164)
        Me.dtDetail.TabIndex = 66
        '
        'btn5Clients
        '
        Me.btn5Clients.Location = New System.Drawing.Point(35, 598)
        Me.btn5Clients.Name = "btn5Clients"
        Me.btn5Clients.Size = New System.Drawing.Size(97, 23)
        Me.btn5Clients.TabIndex = 69
        Me.btn5Clients.Text = "Top 5 Clientes"
        Me.btn5Clients.UseVisualStyleBackColor = True
        '
        'btn10Products
        '
        Me.btn10Products.Location = New System.Drawing.Point(138, 598)
        Me.btn10Products.Name = "btn10Products"
        Me.btn10Products.Size = New System.Drawing.Size(125, 23)
        Me.btn10Products.TabIndex = 70
        Me.btn10Products.Text = "Top 10 Productos"
        Me.btn10Products.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(269, 598)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(97, 23)
        Me.Button1.TabIndex = 71
        Me.Button1.Text = "Top 5 Marcas"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnProducts
        '
        Me.btnProducts.Location = New System.Drawing.Point(372, 598)
        Me.btnProducts.Name = "btnProducts"
        Me.btnProducts.Size = New System.Drawing.Size(167, 23)
        Me.btnProducts.TabIndex = 72
        Me.btnProducts.Text = "Productos Comprados en"
        Me.btnProducts.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(26, 124)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 13)
        Me.Label9.TabIndex = 73
        Me.Label9.Text = "Lugar"
        '
        'cbPlace
        '
        Me.cbPlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPlace.FormattingEnabled = True
        Me.cbPlace.Location = New System.Drawing.Point(99, 124)
        Me.cbPlace.Name = "cbPlace"
        Me.cbPlace.Size = New System.Drawing.Size(178, 21)
        Me.cbPlace.TabIndex = 74
        '
        'bill_layout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 646)
        Me.Controls.Add(Me.cbPlace)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.btnProducts)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn10Products)
        Me.Controls.Add(Me.btn5Clients)
        Me.Controls.Add(Me.btnEditDetail)
        Me.Controls.Add(Me.btnDeleteDetail)
        Me.Controls.Add(Me.dtDetail)
        Me.Controls.Add(Me.btnSaveDetail)
        Me.Controls.Add(Me.txtQuantity)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cbClients)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.calendar)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.cbProducts)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cbBills)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.dtBills)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtHeader)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Name = "bill_layout"
        Me.Text = "Facturas"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dtBills, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnBack As Button
    Friend WithEvents cbProducts As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cbBills As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents dtBills As DataGridView
    Friend WithEvents btnSave As Button
    Friend WithEvents txtHeader As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents calendar As MonthCalendar
    Friend WithEvents Label3 As Label
    Friend WithEvents cbClients As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtQuantity As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btnSaveDetail As Button
    Friend WithEvents btnEditDetail As Button
    Friend WithEvents btnDeleteDetail As Button
    Friend WithEvents dtDetail As DataGridView
    Friend WithEvents btn5Clients As Button
    Friend WithEvents btn10Products As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents btnProducts As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents cbPlace As ComboBox
End Class
