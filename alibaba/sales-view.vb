﻿Imports MySql.Data.MySqlClient

Public Class sales_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub sales_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        LoadBills()
    End Sub

    Private Sub LoadBills()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM factura", connection)
            READER = COMMAND.ExecuteReader
            cbBills.ValueMember = "Key"
            cbBills.DisplayMember = "Value"

            Dim cbBillsValues As New Dictionary(Of String, String)

            While READER.Read
                Dim billHeader = READER.GetString("Encabezado")
                Dim id = READER.GetString("idFactura")

                cbBillsValues.Add(id, billHeader)
            End While

            cbBills.DataSource = New BindingSource(cbBillsValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub cbBills_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbBills.SelectedIndexChanged
        Dim billId = cbBills.SelectedValue.ToString

        Try
            connection.Close()
            connection.Open()
            Dim tableDetail As New DataTable()
            Dim adapterDetail As New MySqlDataAdapter("SELECT df.idDetalleFactura, f.Encabezado, p.Producto, df.Cantidad FROM detallefactura as df INNER JOIN producto as p ON df.Producto_idProducto = p.idProducto INNER JOIN factura as f ON df.Factura_idFactura = f.idFactura WHERE df.Factura_idFactura = " + billId, connection)
            adapterDetail.Fill(tableDetail)
            dtDetail.DataSource = tableDetail
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnGoSales_Click(sender As Object, e As EventArgs) Handles btnGoSales.Click
        bill_layout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class