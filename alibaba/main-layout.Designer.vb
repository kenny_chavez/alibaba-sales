﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainLayout
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HomeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnProducts = New System.Windows.Forms.Button()
        Me.btnCategories = New System.Windows.Forms.Button()
        Me.btnMarcas = New System.Windows.Forms.Button()
        Me.btnClients = New System.Windows.Forms.Button()
        Me.btnVentas = New System.Windows.Forms.Button()
        Me.btnShowProducts = New System.Windows.Forms.Button()
        Me.btnShowCategories = New System.Windows.Forms.Button()
        Me.btnShowMarcas = New System.Windows.Forms.Button()
        Me.btnShowClientes = New System.Windows.Forms.Button()
        Me.btnShowVentas = New System.Windows.Forms.Button()
        Me.btnConnectDB = New System.Windows.Forms.Button()
        Me.lblStatusConnection = New System.Windows.Forms.Label()
        Me.btnPlaces = New System.Windows.Forms.Button()
        Me.btnShowPlaces = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HomeToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(800, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HomeToolStripMenuItem
        '
        Me.HomeToolStripMenuItem.Name = "HomeToolStripMenuItem"
        Me.HomeToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.HomeToolStripMenuItem.Text = "Home"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(31, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(214, 25)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Area de Administración"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(316, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 25)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Gestión de Ventas"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(603, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(164, 25)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Área de Reportes"
        '
        'btnProducts
        '
        Me.btnProducts.Location = New System.Drawing.Point(64, 75)
        Me.btnProducts.Name = "btnProducts"
        Me.btnProducts.Size = New System.Drawing.Size(141, 23)
        Me.btnProducts.TabIndex = 4
        Me.btnProducts.Text = "Administrar Productos"
        Me.btnProducts.UseVisualStyleBackColor = True
        '
        'btnCategories
        '
        Me.btnCategories.Location = New System.Drawing.Point(64, 104)
        Me.btnCategories.Name = "btnCategories"
        Me.btnCategories.Size = New System.Drawing.Size(141, 23)
        Me.btnCategories.TabIndex = 5
        Me.btnCategories.Text = "Administrar Categorias"
        Me.btnCategories.UseVisualStyleBackColor = True
        '
        'btnMarcas
        '
        Me.btnMarcas.Location = New System.Drawing.Point(64, 133)
        Me.btnMarcas.Name = "btnMarcas"
        Me.btnMarcas.Size = New System.Drawing.Size(141, 23)
        Me.btnMarcas.TabIndex = 6
        Me.btnMarcas.Text = "Administrar Marcas"
        Me.btnMarcas.UseVisualStyleBackColor = True
        '
        'btnClients
        '
        Me.btnClients.Location = New System.Drawing.Point(64, 162)
        Me.btnClients.Name = "btnClients"
        Me.btnClients.Size = New System.Drawing.Size(141, 23)
        Me.btnClients.TabIndex = 7
        Me.btnClients.Text = "Administrar Clientes"
        Me.btnClients.UseVisualStyleBackColor = True
        '
        'btnVentas
        '
        Me.btnVentas.Location = New System.Drawing.Point(335, 114)
        Me.btnVentas.Name = "btnVentas"
        Me.btnVentas.Size = New System.Drawing.Size(141, 23)
        Me.btnVentas.TabIndex = 8
        Me.btnVentas.Text = "Gestionar Ventas"
        Me.btnVentas.UseVisualStyleBackColor = True
        '
        'btnShowProducts
        '
        Me.btnShowProducts.Location = New System.Drawing.Point(608, 75)
        Me.btnShowProducts.Name = "btnShowProducts"
        Me.btnShowProducts.Size = New System.Drawing.Size(141, 23)
        Me.btnShowProducts.TabIndex = 9
        Me.btnShowProducts.Text = "Ver Productos"
        Me.btnShowProducts.UseVisualStyleBackColor = True
        '
        'btnShowCategories
        '
        Me.btnShowCategories.Location = New System.Drawing.Point(608, 104)
        Me.btnShowCategories.Name = "btnShowCategories"
        Me.btnShowCategories.Size = New System.Drawing.Size(141, 23)
        Me.btnShowCategories.TabIndex = 10
        Me.btnShowCategories.Text = "Ver Categorias"
        Me.btnShowCategories.UseVisualStyleBackColor = True
        '
        'btnShowMarcas
        '
        Me.btnShowMarcas.Location = New System.Drawing.Point(608, 133)
        Me.btnShowMarcas.Name = "btnShowMarcas"
        Me.btnShowMarcas.Size = New System.Drawing.Size(141, 23)
        Me.btnShowMarcas.TabIndex = 11
        Me.btnShowMarcas.Text = "Ver Marcas"
        Me.btnShowMarcas.UseVisualStyleBackColor = True
        '
        'btnShowClientes
        '
        Me.btnShowClientes.Location = New System.Drawing.Point(608, 162)
        Me.btnShowClientes.Name = "btnShowClientes"
        Me.btnShowClientes.Size = New System.Drawing.Size(141, 23)
        Me.btnShowClientes.TabIndex = 12
        Me.btnShowClientes.Text = "Ver Clientes"
        Me.btnShowClientes.UseVisualStyleBackColor = True
        '
        'btnShowVentas
        '
        Me.btnShowVentas.Location = New System.Drawing.Point(608, 220)
        Me.btnShowVentas.Name = "btnShowVentas"
        Me.btnShowVentas.Size = New System.Drawing.Size(141, 23)
        Me.btnShowVentas.TabIndex = 13
        Me.btnShowVentas.Text = "Ver Ventas"
        Me.btnShowVentas.UseVisualStyleBackColor = True
        '
        'btnConnectDB
        '
        Me.btnConnectDB.Location = New System.Drawing.Point(64, 415)
        Me.btnConnectDB.Name = "btnConnectDB"
        Me.btnConnectDB.Size = New System.Drawing.Size(118, 23)
        Me.btnConnectDB.TabIndex = 14
        Me.btnConnectDB.Text = "Conectar a MySql"
        Me.btnConnectDB.UseVisualStyleBackColor = True
        '
        'lblStatusConnection
        '
        Me.lblStatusConnection.AutoSize = True
        Me.lblStatusConnection.Location = New System.Drawing.Point(206, 420)
        Me.lblStatusConnection.Name = "lblStatusConnection"
        Me.lblStatusConnection.Size = New System.Drawing.Size(116, 13)
        Me.lblStatusConnection.TabIndex = 15
        Me.lblStatusConnection.Text = "Estado: Desconectado"
        '
        'btnPlaces
        '
        Me.btnPlaces.Location = New System.Drawing.Point(64, 191)
        Me.btnPlaces.Name = "btnPlaces"
        Me.btnPlaces.Size = New System.Drawing.Size(141, 23)
        Me.btnPlaces.TabIndex = 16
        Me.btnPlaces.Text = "Administrar Lugares"
        Me.btnPlaces.UseVisualStyleBackColor = True
        '
        'btnShowPlaces
        '
        Me.btnShowPlaces.Location = New System.Drawing.Point(608, 191)
        Me.btnShowPlaces.Name = "btnShowPlaces"
        Me.btnShowPlaces.Size = New System.Drawing.Size(141, 23)
        Me.btnShowPlaces.TabIndex = 17
        Me.btnShowPlaces.Text = "Ver Lugares"
        Me.btnShowPlaces.UseVisualStyleBackColor = True
        '
        'MainLayout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnShowPlaces)
        Me.Controls.Add(Me.btnPlaces)
        Me.Controls.Add(Me.lblStatusConnection)
        Me.Controls.Add(Me.btnConnectDB)
        Me.Controls.Add(Me.btnShowVentas)
        Me.Controls.Add(Me.btnShowClientes)
        Me.Controls.Add(Me.btnShowMarcas)
        Me.Controls.Add(Me.btnShowCategories)
        Me.Controls.Add(Me.btnShowProducts)
        Me.Controls.Add(Me.btnVentas)
        Me.Controls.Add(Me.btnClients)
        Me.Controls.Add(Me.btnMarcas)
        Me.Controls.Add(Me.btnCategories)
        Me.Controls.Add(Me.btnProducts)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MainLayout"
        Me.Text = "Alibaba"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HomeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnProducts As Button
    Friend WithEvents btnCategories As Button
    Friend WithEvents btnMarcas As Button
    Friend WithEvents btnClients As Button
    Friend WithEvents btnVentas As Button
    Friend WithEvents btnShowProducts As Button
    Friend WithEvents btnShowCategories As Button
    Friend WithEvents btnShowMarcas As Button
    Friend WithEvents btnShowClientes As Button
    Friend WithEvents btnShowVentas As Button
    Friend WithEvents btnConnectDB As Button
    Friend WithEvents lblStatusConnection As Label
    Friend WithEvents btnPlaces As Button
    Friend WithEvents btnShowPlaces As Button
End Class
