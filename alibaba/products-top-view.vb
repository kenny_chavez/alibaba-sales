﻿Imports MySql.Data.MySqlClient

Public Class products_top_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub products_top_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT p.idProducto, p.Producto, p.Precio, p.Stock, p.Descripcion, c.Categoria, m.Marca, COUNT(df.Producto_idProducto) AS 'Ventas' FROM detallefactura as df
            INNER JOIN producto as p ON df.Producto_idProducto = p.idProducto
            INNER JOIN categoria as c ON p.Categoria_idCategoria = c.idCategoria
            INNER JOIN marca as m ON p.Marca_idMarca = m.idMarca
            GROUP BY p.idProducto ORDER BY Ventas DESC LIMIT 10", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class