﻿Imports MySql.Data.MySqlClient

Public Class bill_layout

    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub bill_layout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshData()
        refreshDataDetail()
    End Sub

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT f.idFactura, f.Encabezado, f.Total, c.Nombre, l.Nombre as Lugar FROM factura as f
        INNER JOIN cliente as c ON f.Cliente_idCliente = c.idCliente INNER JOIN lugar as l ON f.Lugar_idLugar = l.idLugar", connection)

        adapter.Fill(table)
        dtBills.DataSource = table

        LoadClients()
        LoadBills()
        LoadProducto()
        LoadPlace()
    End Sub

    Private Sub refreshDataDetail()
        Dim tableDetail As New DataTable()
        Dim adapterDetail As New MySqlDataAdapter("SELECT df.idDetalleFactura, f.Encabezado, p.Producto, df.Cantidad FROM detallefactura as df INNER JOIN producto as p ON df.Producto_idProducto = p.idProducto INNER JOIN factura as f ON df.Factura_idFactura = f.idFactura", connection)
        adapterDetail.Fill(tableDetail)
        dtDetail.DataSource = tableDetail
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub LoadClients()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM cliente", connection)
            READER = COMMAND.ExecuteReader
            cbClients.ValueMember = "Key"
            cbClients.DisplayMember = "Value"

            Dim cbCategorieValues As New Dictionary(Of String, String)

            While READER.Read
                Dim categorieName = READER.GetString("Nombre")
                Dim id = READER.GetString("idCliente")

                cbCategorieValues.Add(id, categorieName)
            End While

            cbClients.DataSource = New BindingSource(cbCategorieValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub LoadPlace()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM lugar", connection)
            READER = COMMAND.ExecuteReader
            cbPlace.ValueMember = "Key"
            cbPlace.DisplayMember = "Value"

            Dim cbPlaceValues As New Dictionary(Of String, String)

            While READER.Read
                Dim placeName = READER.GetString("Nombre")
                Dim id = READER.GetString("idLugar")

                cbPlaceValues.Add(id, placeName)
            End While

            cbPlace.DataSource = New BindingSource(cbPlaceValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub LoadBills()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM factura", connection)
            READER = COMMAND.ExecuteReader
            cbBills.ValueMember = "Key"
            cbBills.DisplayMember = "Value"

            Dim cbBillsValues As New Dictionary(Of String, String)

            While READER.Read
                Dim billHeader = READER.GetString("Encabezado")
                Dim id = READER.GetString("idFactura")

                cbBillsValues.Add(id, billHeader)
            End While

            cbBills.DataSource = New BindingSource(cbBillsValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub LoadProducto()
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM producto", connection)
            READER = COMMAND.ExecuteReader
            cbProducts.ValueMember = "Key"
            cbProducts.DisplayMember = "Value"

            Dim cbProductsValues As New Dictionary(Of String, String)

            While READER.Read
                Dim productName = READER.GetString("Producto")
                Dim id = READER.GetString("idProducto")

                cbProductsValues.Add(id, productName)
            End While

            cbProducts.DataSource = New BindingSource(cbProductsValues, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddBill = "INSERT INTO factura(Encabezado, Total, Cliente_idCliente, Lugar_idLugar, Fecha) VALUES 
        ('" + txtHeader.Text + "', 0, " + cbClients.SelectedValue.ToString + "," + cbPlace.SelectedValue.ToString + ", '" + calendar.SelectionStart.ToString("yyyy-MM-dd") + "')"
        MakeQuery(QueryAddBill)
        refreshData()

        txtHeader.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtBills.SelectedRows.Count > 0 Then
            For i As Integer = dtBills.SelectedRows.Count - 1 To 0 Step -1

                Dim ProductId As String

                ProductId = dtBills.Rows(dtBills.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM factura WHERE idFactura = " + ProductId
                MakeQuery(queryDelete)

                dtBills.Rows.RemoveAt(dtBills.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtBills.ReadOnly = False
    End Sub

    Private Sub dtBills_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtBills.CellValueChanged
        Dim id As String = dtBills.CurrentRow.Cells(0).Value
        Dim value As String = dtBills.CurrentCell.Value
        Dim x As String = dtBills.CurrentCellAddress.X
        Dim queryEdit As String = "UPDATE factura SET "

        If x.Equals("1") Then
            queryEdit += " Encabezado='" + value + "' "
        End If

        If x.Equals("2") Then
            queryEdit += " Total=" + value + " "
        End If

        If x.Equals("3") Then
            queryEdit += " Cliente_idCliente=" + value + " "
        End If

        queryEdit += " WHERE idFactura = " + id

        If x <> "0" And x <> "2" Then
            MakeQuery(queryEdit)
        End If

        refreshData()
    End Sub

    Private Sub btnSaveDetail_Click(sender As Object, e As EventArgs) Handles btnSaveDetail.Click
        Dim QueryAddDetail = "INSERT INTO detallefactura(Cantidad, Producto_idProducto, Factura_idFactura) VALUES 
        (" + txtQuantity.Text + ", " + cbProducts.SelectedValue.ToString + ", " + cbBills.SelectedValue.ToString + ")"
        Dim status As Boolean = updateQuantity(cbProducts.SelectedValue.ToString, txtQuantity.Text)

        If (status) Then
            MakeQuery(QueryAddDetail)
            refreshData()
            refreshDataDetail()
        Else
            MessageBox.Show("La cantidad debe ser menor o igual a la cantidad del stock.")
        End If

        txtQuantity.Text = "0"
    End Sub

    Private Function updateQuantity(productId As String, quantity As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand("SELECT * FROM producto WHERE idProducto = " + productId, connection)
            READER = COMMAND.ExecuteReader
            Dim stock As String = ""

            While READER.Read
                stock = READER.GetString("Stock")
            End While

            Dim newQuantity As Int32 = Convert.ToInt32(stock) - Convert.ToInt32(quantity)

            If newQuantity >= 0 Then
                connection.Close()
                Dim queryUpdate As String = "UPDATE producto SET Stock = " + newQuantity.ToString + " WHERE idProducto = " + productId
                MakeQuery(queryUpdate)

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try

        Return False
    End Function

    Private Sub btnDeleteDetail_Click(sender As Object, e As EventArgs) Handles btnDeleteDetail.Click
        If dtDetail.SelectedRows.Count > 0 Then
            For i As Integer = dtDetail.SelectedRows.Count - 1 To 0 Step -1

                Dim detailId As String

                detailId = dtDetail.Rows(dtDetail.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM detallefactura WHERE idDetalleFactura = " + detailId
                MakeQuery(queryDelete)

                dtDetail.Rows.RemoveAt(dtDetail.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEditDetail_Click(sender As Object, e As EventArgs) Handles btnEditDetail.Click
        dtDetail.ReadOnly = False
    End Sub

    Private Sub dtDetail_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtDetail.CellValueChanged
        Dim id As String = dtDetail.CurrentRow.Cells(0).Value
        Dim value As String = dtDetail.CurrentCell.Value
        Dim x As String = dtDetail.CurrentCellAddress.X
        Dim queryEdit As String = "UPDATE detallefactura SET "

        If x.Equals("1") Then
            queryEdit += " Factura_idFactura=" + value + " "
        End If

        If x.Equals("2") Then
            queryEdit += " Producto_idProducto=" + value + " "
        End If

        If x.Equals("3") Then
            queryEdit += " Cantidad=" + value + " "
        End If

        queryEdit += " WHERE idDetalleFactura = " + id

        If x <> "0" Then
            MakeQuery(queryEdit)
        End If

        refreshData()
        refreshDataDetail()
    End Sub

    Private Sub btn5Clients_Click(sender As Object, e As EventArgs) Handles btn5Clients.Click
        clients_top_view.Show()
    End Sub

    Private Sub btn10Products_Click(sender As Object, e As EventArgs) Handles btn10Products.Click
        products_top_view.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        brands_top_view.Show()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub

    Private Sub btnProducts_Click(sender As Object, e As EventArgs) Handles btnProducts.Click
        purchase_products.Show()
    End Sub
End Class