﻿Imports MySql.Data.MySqlClient

Public Class purchase_products
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub purchase_products_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim placeName = txtPlace.Text.ToLower
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT p.idProducto, p.Producto, p.Descripcion, p.Precio, p.Stock FROM producto as p
        WHERE p.idProducto in (SELECT df.Producto_idProducto FROM detallefactura as df WHERE df.Factura_idFactura in (SELECT f.idFactura FROM factura as f WHERE f.Lugar_idLugar in (SELECT l.idLugar FROM lugar as l WHERE Nombre = '" + placeName + "')))", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub
End Class