﻿Imports MySql.Data.MySqlClient

Public Class MainLayout
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;password=")

    Private Sub btnCategories_Click(sender As Object, e As EventArgs) Handles btnCategories.Click
        Categories.Show()
    End Sub

    Private Sub btnConnectDB_Click(sender As Object, e As EventArgs) Handles btnConnectDB.Click
        Try
            connection.Open()
            lblStatusConnection.Text = "Estado: Conectado"
        Catch ex As Exception
            lblStatusConnection.Text = "Estado: Error de conexión"
        End Try
    End Sub

    Private Sub btnMarcas_Click(sender As Object, e As EventArgs) Handles btnMarcas.Click
        brands_layout.Show()
    End Sub

    Private Sub MainLayout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Categories.Hide()
        brands_layout.Hide()
        clients_layout.Hide()
        products_layout.Hide()
    End Sub

    Private Sub btnClients_Click(sender As Object, e As EventArgs) Handles btnClients.Click
        clients_layout.Show()
    End Sub

    Private Sub btnProducts_Click(sender As Object, e As EventArgs) Handles btnProducts.Click
        products_layout.Show()
    End Sub

    Private Sub btnVentas_Click(sender As Object, e As EventArgs) Handles btnVentas.Click
        bill_layout.Show()
    End Sub

    Private Sub btnShowProducts_Click(sender As Object, e As EventArgs) Handles btnShowProducts.Click
        products_view.Show()
    End Sub

    Private Sub btnShowCategories_Click(sender As Object, e As EventArgs) Handles btnShowCategories.Click
        categorie_view.Show()
    End Sub

    Private Sub btnShowMarcas_Click(sender As Object, e As EventArgs) Handles btnShowMarcas.Click
        brands_view.Show()
    End Sub

    Private Sub btnShowClientes_Click(sender As Object, e As EventArgs) Handles btnShowClientes.Click
        clients_view.Show()
    End Sub

    Private Sub btnShowVentas_Click(sender As Object, e As EventArgs) Handles btnShowVentas.Click
        sales_view.Show()
    End Sub

    Private Sub btnPlaces_Click(sender As Object, e As EventArgs) Handles btnPlaces.Click
        place_layout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub btnShowPlaces_Click(sender As Object, e As EventArgs) Handles btnShowPlaces.Click
        place_view.Show()
    End Sub
End Class
