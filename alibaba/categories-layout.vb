﻿Imports MySql.Data.MySqlClient

Public Class Categories
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub Categories_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshDataCategories()
    End Sub

    Private Sub refreshDataCategories()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT * FROM categoria", connection)

        adapter.Fill(table)
        dtCategories.DataSource = table
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddCategorie = "INSERT INTO categoria(Categoria) VALUES ('" + txtCategorieName.Text + "')"
        MakeQuery(QueryAddCategorie)
        refreshDataCategories()
        txtCategorieName.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtCategories.SelectedRows.Count > 0 Then
            For i As Integer = dtCategories.SelectedRows.Count - 1 To 0 Step -1

                Dim categorieId As String

                categorieId = dtCategories.Rows(dtCategories.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM categoria WHERE idCategoria = " + categorieId
                MakeQuery(queryDelete)

                dtCategories.Rows.RemoveAt(dtCategories.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtCategories.ReadOnly = False
    End Sub

    Private Sub dtCategories_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtCategories.CellValueChanged
        Dim id As String = dtCategories.CurrentRow.Cells(0).Value
        Dim categorie As String = dtCategories.CurrentCell.Value
        Dim queryEdit As String = "UPDATE categoria SET Categoria='" + categorie + "' WHERE idCategoria = " + id
        MakeQuery(queryEdit)
        refreshDataCategories()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class