﻿Imports MySql.Data.MySqlClient

Public Class clients_layout
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub clients_layout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshData()
    End Sub

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT idCliente, Nombre, NIT, Direccion FROM cliente", connection)

        adapter.Fill(table)
        dtClients.DataSource = table
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddBrand = "INSERT INTO cliente(Nombre, NIT, Direccion) 
        VALUES ('" + txtName.Text + "', '" + txtNit.Text + "', '" + txtAddress.Text + "')"
        MakeQuery(QueryAddBrand)
        refreshData()

        txtName.Text = ""
        txtNit.Text = ""
        txtAddress.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtClients.SelectedRows.Count > 0 Then
            For i As Integer = dtClients.SelectedRows.Count - 1 To 0 Step -1

                Dim clientId As String

                clientId = dtClients.Rows(dtClients.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM cliente WHERE idCliente = " + clientId
                MakeQuery(queryDelete)

                dtClients.Rows.RemoveAt(dtClients.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtClients.ReadOnly = False
    End Sub

    Private Sub dtClients_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtClients.CellValueChanged
        Dim id As String = dtClients.CurrentRow.Cells(0).Value
        Dim value As String = dtClients.CurrentCell.Value
        Dim x As String = dtClients.CurrentCellAddress.X
        Dim queryEdit As String = "UPDATE cliente SET "

        If x.Equals("1") Then
            queryEdit += " Nombre='" + value + "' "
        End If

        If x.Equals("2") Then
            queryEdit += " NIT='" + value + "' "
        End If

        If x.Equals("3") Then
            queryEdit += " Direccion='" + value + "' "
        End If

        queryEdit += " WHERE idCliente = " + id

        If x <> "0" Then
            MakeQuery(queryEdit)
        End If

        refreshData()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub

End Class