﻿Imports MySql.Data.MySqlClient

Public Class place_layout
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub place_layout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshDataPlaces()
    End Sub

    Private Sub refreshDataPlaces()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT * FROM lugar", connection)

        adapter.Fill(table)
        dtPlaces.DataSource = table
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddPlace = "INSERT INTO lugar(Nombre) VALUES ('" + txtPlaceName.Text + "')"
        MakeQuery(QueryAddPlace)
        refreshDataPlaces()
        txtPlaceName.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtPlaces.SelectedRows.Count > 0 Then
            For i As Integer = dtPlaces.SelectedRows.Count - 1 To 0 Step -1

                Dim placeId As String

                placeId = dtPlaces.Rows(dtPlaces.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM lugar WHERE idLugar = " + placeId
                MakeQuery(queryDelete)

                dtPlaces.Rows.RemoveAt(dtPlaces.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtPlaces.ReadOnly = False
    End Sub

    Private Sub dtCategories_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtPlaces.CellValueChanged
        Dim id As String = dtPlaces.CurrentRow.Cells(0).Value
        Dim place As String = dtPlaces.CurrentCell.Value
        Dim queryEdit As String = "UPDATE lugar SET Nombre='" + place + "' WHERE idLugar = " + id
        MakeQuery(queryEdit)
        refreshDataPlaces()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class