﻿Imports MySql.Data.MySqlClient

Public Class clients_top_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub clients_top_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT c.idCliente, c.Nombre, c.NIT, c.Direccion, COUNT(f.Cliente_idCliente) as 'Facturas' FROM factura as f
            INNER JOIN cliente as c ON f.Cliente_idCliente = c.idCliente
            GROUP BY F.Cliente_idCliente ORDER BY Facturas DESC LIMIT 5", connection)

        adapter.Fill(table)
        dtClients.DataSource = table
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class