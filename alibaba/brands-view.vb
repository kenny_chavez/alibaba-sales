﻿Imports MySql.Data.MySqlClient

Public Class brands_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub brands_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshData()
    End Sub

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT * FROM marca", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub

    Private Sub btnGoBrands_Click(sender As Object, e As EventArgs) Handles btnGoBrands.Click
        brands_layout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class