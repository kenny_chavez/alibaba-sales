﻿Imports MySql.Data.MySqlClient

Public Class products_view
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT p.idProducto, p.Producto, p.Precio, p.Stock, p.Descripcion, c.Categoria, m.Marca FROM producto as p
            INNER JOIN categoria c ON p.Categoria_idCategoria = c.idCategoria 
            INNER JOIN marca m ON p.Marca_idMarca = m.idMarca", connection)

        adapter.Fill(table)
        dtProducts.DataSource = table
    End Sub

    Private Sub products_view_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshData()
    End Sub

    Private Sub btnGoProducts_Click(sender As Object, e As EventArgs) Handles btnGoProducts.Click
        products_layout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class