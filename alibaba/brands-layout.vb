﻿Imports MySql.Data.MySqlClient

Public Class brands_layout
    Dim connection As New MySqlConnection("datasource=localhost;port=3306;username=root;database=alibaba;password=")
    Dim COMMAND As MySqlCommand

    Private Sub brands_layout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MainLayout.Hide()
        refreshData()
    End Sub

    Private Sub refreshData()
        Dim table As New DataTable()
        Dim adapter As New MySqlDataAdapter("SELECT * FROM marca", connection)

        adapter.Fill(table)
        dtBrands.DataSource = table
    End Sub

    Private Sub MakeQuery(query As String)
        Try
            connection.Open()
            Dim READER As MySqlDataReader
            COMMAND = New MySqlCommand(query, connection)
            READER = COMMAND.ExecuteReader
            MessageBox.Show("Se ha ejecutado correctamente")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            connection.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim QueryAddBrand = "INSERT INTO marca(Marca) VALUES ('" + txtBrandName.Text + "')"
        MakeQuery(QueryAddBrand)
        refreshData()

        txtBrandName.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If dtBrands.SelectedRows.Count > 0 Then
            For i As Integer = dtBrands.SelectedRows.Count - 1 To 0 Step -1

                Dim brandId As String

                brandId = dtBrands.Rows(dtBrands.SelectedRows(i).Index).Cells(0).Value
                Dim queryDelete As String = "DELETE FROM marca WHERE idMarca = " + brandId
                MakeQuery(queryDelete)

                dtBrands.Rows.RemoveAt(dtBrands.SelectedRows(i).Index)
            Next
        Else
            MessageBox.Show("No rows selected.")
        End If
    End Sub

    Private Sub dtBrands_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtBrands.CellValueChanged
        Dim id As String = dtBrands.CurrentRow.Cells(0).Value
        Dim brand As String = dtBrands.CurrentCell.Value
        Dim queryEdit As String = "UPDATE marca SET Marca='" + brand + "' WHERE idMarca = " + id
        MakeQuery(queryEdit)
        refreshData()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        dtBrands.ReadOnly = False
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        MainLayout.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        MainLayout.Close()
    End Sub
End Class